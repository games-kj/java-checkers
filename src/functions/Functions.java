/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author Johannes
 */
public class Functions {

    // Parse board given as json string. Mainly useful for API usage
    // TODO: Documentation of json scheme, i.e. how the board information should be formatted.
    public static int[][] parseCharBoard(String json) {
        
        int[][] res;
        
        json = json.replace("\"", "");
        json = json.replace("'", "");
        json = json.replace(" ", "");
        
        String[] jsonCols = json.split("],\\[");
        res = new int[jsonCols.length][];
        
        String[] jsonFields;
        int nrCol = 0;
        int nrField;
        for (String jsonCol : jsonCols) {
            jsonCol = jsonCol.replace("[", "");
            jsonCol = jsonCol.replace("]", "");
            jsonFields = jsonCol.split(",");
            res[nrCol] = new int[jsonFields.length];
            nrField = 0;
            for (String jsonField : jsonFields) {
                switch (jsonField) {
                    case "w":
                        res[nrCol][nrField] = 1;
                        break;
                    case "b":
                        res[nrCol][nrField] = -1;
                        break;
                    case "W":
                        res[nrCol][nrField] = 2;
                        break;
                    case "B":
                        res[nrCol][nrField] = -2;
                        break;
                    default:
                        res[nrCol][nrField] = 0;
                }
                nrField++;
            }
            nrCol++;
        }
        
        return res;
    }
}
