/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.List;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Allows for different agents (e.g. human, computer with various strategies, ...) to be used in a Game or in the api
public class Agent {
    
    public Board board;
    public int playerNr;
    
    public void init(Board board, int playerNr) {
        
        this.board = board;
        this.playerNr = playerNr;
    }

    // Choose a move to do next
    public List<Move> chooseMove() {
        
        List<Move> res = new ArrayList<>();
        
        Move move = new Move(0, 0, 0, 0);
        res.add(move);
        
        return res;
    }
}
