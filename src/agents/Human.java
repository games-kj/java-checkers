/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Agent that reads the next move from a CLI user
public class Human extends Agent {

    @Override
    public List<Move> chooseMove() {
        
        List<Move> res = new ArrayList<>();
        
        System.out.println("Player " + this.playerNr + ", please enter your move: ");
        
        String[] movesText;
        int stringPos;
        int fromX;
        int fromY;
        int toX;
        int toY;
        boolean isCapture;
        boolean failed;

        // Keep reading moves until a valid one is put in
        while (true) {
            
            failed = false;
            res.clear();

            // Read input

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String readMove;
            
            try {
                readMove = reader.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Human.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Error while reading input, please try again: ");
                continue;
            }

            // Adjustments to reach more predictable format (lowercase, space-terminated)

            readMove = readMove.toLowerCase();
            
            if (readMove.length() > 0 && readMove.charAt(readMove.length() - 1) != ' ') readMove += " ";
            readMove = readMove.toLowerCase();

            // Check if move is in valid format
            if (!readMove.matches("^([a-z][0-9]x?[a-z][0-9] )+$")) {
                System.out.println("Invalid input format. Expecting <startcoords><endcoords> in chessboard coordinate format (e.g. a3b4 would be a move), or <startcoords>x<endcoords> in case of a capture, or multiple of such moves separated by a space (' ') in case of combined moves. Please try again: ");
                continue;
            }

            // Singular moves in case a combination of moves was given
            movesText = readMove.split(" ");

            // Parse singular moves and combine them to a result as long as they are valid
            for (String moveText : movesText) {
                
                stringPos = 0;
                fromX = Character.getNumericValue(moveText.charAt(stringPos)) - 10;
                stringPos++;
                fromY = Character.getNumericValue(moveText.charAt(stringPos)) - 1;
                stringPos++;
                if (moveText.charAt(stringPos) == 'x') {
                    isCapture = true;
                    stringPos++;
                }
                else {
                    isCapture = false;
                }
                toX = Character.getNumericValue(moveText.charAt(stringPos)) - 10;
                stringPos++;
                toY = Character.getNumericValue(moveText.charAt(stringPos)) - 1;
                
                if (fromX < 0 || fromX >= this.board.size || fromY < 0 || fromY >= this.board.size || toX < 0 || toX >= this.board.size || toY < 0 || toY >= this.board.size) {
                    failed = true;
                    break;
                }
                
                res.add(new Move(fromX, fromY, toX, toY, isCapture));
            }
            
            if (failed) continue;
    
            return res;
        }
    }
}
