/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Agent implementing the MinMax algorithm with some tweaks
public class AgentMinMax1 extends Agent {
    
    public int maxDepth = 2;
    public int maxDepthInitial = 2;
    public long allowedMillis = 5000;
    public Instant timeStart;
    public boolean timeUp;
    public int transHits;
    public HashMap<String, Integer> transpositionTable;
    
    public boolean checkTimeEachMove = true;
    public boolean updateEachMove = true;
    public boolean acceptFirstWinningMove = false;
    
    public List<Move> chosenMove;
    public int chosenMoveEstimation;
    
    public final int WORTH_PAWN = 100;
    public final int WORTH_QUEEN = 900;
    public final int BOUND_WIN = 999999;
    public final int SIGNAL_WIN = 9999999;
    public final int SIGNAL_IMPOSSIBLE = 9999999;
    public final int BONUS_DIAGONAL = 5;
    public final int BONUS_BASELINE = 6;
    public final int BONUS_PROGRESS = 1;
    public final int BONUS_CENTER = 1;

    // Heuristic value estimation
    public int evaluateBoard(Board board) {

        // If no moves are possible, the current player looses
        if (board.getMoves().isEmpty()) {
            return (board.turn > 0 ? -SIGNAL_WIN : SIGNAL_WIN);
        }
        
        int res = 0;

        // Scan fields for pieces and evaluate them
        for (int x = 0; x < board.size; x++) {
            for (int y = 0; y < board.size; y++) {

                // Assign certain value to pieces. In case of a pawn (queens can move quickly to any position anyway), reward progress, center proximity, baseline protection and diagonal piece protection
                switch (board.fields[x][y]) {
                    case Board.PIECE_NONE:
                        continue;
                    case Board.PIECE_PAWN_WHITE:
                        res += (this.WORTH_PAWN + (y * this.BONUS_PROGRESS) + (((this.board.size / 2) - Math.abs((this.board.size / 2) - x)) * this.BONUS_CENTER));
                        if (y == 0) {
                            res += this.BONUS_BASELINE;
                        }
                        else {
                            if (x > 0 && board.fields[x-1][y-1] == Board.PIECE_PAWN_WHITE) res += this.BONUS_DIAGONAL;
                            if (x < this.board.size - 1 && board.fields[x+1][y-1] == Board.PIECE_PAWN_WHITE) res += this.BONUS_DIAGONAL;
                        }
                        break;
                    case Board.PIECE_PAWN_BLACK:
                        res -= (this.WORTH_PAWN + ((this.board.size - 1 - y) * this.BONUS_PROGRESS) + (((this.board.size / 2) - Math.abs((this.board.size / 2) - x)) * this.BONUS_CENTER));
                        if (y == this.board.size - 1) {
                            res -= this.BONUS_BASELINE;
                        }
                        else {
                            if (x > 0 && board.fields[x-1][y+1] == Board.PIECE_PAWN_BLACK) res -= this.BONUS_DIAGONAL;
                            if (x < this.board.size - 1 && board.fields[x+1][y+1] == Board.PIECE_PAWN_BLACK) res -= this.BONUS_DIAGONAL;
                        }
                        break;
                    case Board.PIECE_QUEEN_WHITE:
                        res += this.WORTH_QUEEN;
                        break;
                    case Board.PIECE_QUEEN_BLACK:
                        res -= this.WORTH_QUEEN;
                        break;
                    default:
                        System.out.println("=X=X=X=X=X=X= FOUND ILLEGAL PIECE =X=X=X=X=X=X=");
                }
            }
        }
        
        return res;
    }
    
    @Override
    public List<Move> chooseMove() {

        this.transpositionTable = new HashMap<>();
        this.transHits = 0;
        
        Board workboard = new Board(this.board);
        
        this.maxDepth = this.maxDepthInitial;
        
        this.timeStart = Instant.now();
        
        List<Move> res = workboard.getMoves().get(0);
        
        this.chosenMove = workboard.getMoves().get(0);
        this.chosenMoveEstimation = (this.playerNr == 1 ? -this.SIGNAL_IMPOSSIBLE : this.SIGNAL_IMPOSSIBLE);
        
        int estimation;
        
        int nrCalculationRounds = 1;
        this.timeUp = false;
        
        System.out.println("----- AgentMinMax1 Starting Calculation -----");
        System.out.println("Current board evaluation: " + this.evaluateBoard(workboard));

        // If only one move is possible: Simply "choose" that one
        if (workboard.getMoves().size() == 1) {
            return res;
        }
        
        long millisRoundCheck = (this.checkTimeEachMove ? this.allowedMillis : this.allowedMillis / 2);

        // While time is not up and no win or loss has been found: Perform MinMax with increasing depths (starting over in each iteration)
        while (true) {
            transHits = 0;
            
            System.out.println("Starting MinMax Calculation round " + nrCalculationRounds + " with maxDepth=" + this.maxDepth);

            // Invoke MinMax, starting according to player color
            if (this.playerNr == 1) {
                estimation = this.maximize(workboard, 0, this.BOUND_WIN);
            }
            else {
                estimation = this.minimize(workboard, 0, -this.BOUND_WIN);
            }
            System.out.println("Expecting " + estimation);

            // No more time for further iterations
            if (this.timeUp) break;

            // Found a winning strategy => Don't need to search further
            if ((this.playerNr == 1 && estimation >= this.BOUND_WIN) || (this.playerNr == -1 && estimation <= -this.BOUND_WIN)) {
                System.out.println("Quitting after this round since win is already sure");
                break;
            }
            // Found out that there is a winning strategy for the enemy => Just go with best found move
            if ((this.playerNr == -1 && estimation >= this.BOUND_WIN) || (this.playerNr == 1 && estimation <= -this.BOUND_WIN)) {
                System.out.println("Oh no, I will loose, but I'm trying to survive as long as possible");
                break;
            }

            // Clear transposition table for next iteration
            this.transpositionTable.clear();

            // Dig deeper on next iteration
            this.maxDepth++;
            nrCalculationRounds ++;
        }
        
        System.out.println("Got to depth " + this.maxDepth + ", expecting " + this.chosenMoveEstimation + ". TransTable has " + this.transpositionTable.size() + " entries and had " + this.transHits + " hits.");
        return this.chosenMove;
    }

    // TODO: Maybe combine maximize and minimize functions to avoid duplicate code

    // Max-part of MinMax
    public int maximize(Board board, int depth, int foundMin) {
        
        int res = -SIGNAL_IMPOSSIBLE;
        int current;
        List<Move> bestMove = null;
        
        int resultingEvaluation;
        
        String transRep;

        // Go through available next moves (each of which can be a combination, i.e. a list of singular moves)
        for (List<Move> mv : board.getMoves()) {
            
            Board resultingBoard = new Board(board);
            resultingBoard.moveUncheckedComplete(mv);
            
            transRep = transpositionRepresentation(resultingBoard);

            // If move is present in transposition table: Use already calculated value
            // TODO : transHits is just for debugging and development decisions, and should maybe be taken out for production
            if (this.transpositionTable.get(transRep) != null) {
                this.transHits++;
                current = this.transpositionTable.get(transRep);
            }
            else {
                resultingEvaluation = this.evaluateBoard(resultingBoard);

                // If maximum depth is reached or game is decided: Use heuristic function
                if (depth >= this.maxDepth || resultingEvaluation == this.SIGNAL_WIN || resultingEvaluation == -this.SIGNAL_WIN) {
                    current = resultingEvaluation;
                }
                else {
                    // Go to next depth with counterpart function
                    current = this.minimize(resultingBoard, depth + 1, res);
                }

                // Put move into transposition table
                this.transpositionTable.put(transRep, current);
            }

            // If move looks better than previous favorite: Make it new favorite
            if (current > res) {
                res = current;
                bestMove = mv;
                // If we are at depth 0, i.e. moves are actual possible moves, and the chosen move should be updated immediately: Do it
                if (depth == 0 && this.updateEachMove && res > this.chosenMoveEstimation) {
                    this.chosenMove = bestMove;
                    this.chosenMoveEstimation = res;
                }
                // Aplha-Beta-pruning or found winning move
                if (res >= foundMin && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }
            // If we are at depth 0 and the time is up: Wrap it up
            if (depth == 0 && this.checkTimeEachMove && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                if (this.chosenMoveEstimation == this.SIGNAL_IMPOSSIBLE) {
                    this.chosenMove = bestMove;
                    this.chosenMoveEstimation = res;
                }
                this.timeUp = true;
                return res;
            }
        }

        // Went through all moves, time is not up

        // Set chosen move just in case it was not set earlier
        if (depth == 0) {
            this.chosenMove = bestMove;
            this.chosenMoveEstimation = res;
        }

        // If a sure win resp. loss was found: Favor short resp. long remaining game
        if (res == this.SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -this.SIGNAL_WIN) {
            res += depth;
        }

        // Memory saving and trying to avoid unexpected side effects of move caching
        // TODO: Maybe introduce long term estimations for move combinations and using it for sorting after each iteration => Then do not clear moves in depth 0
        board.moves.clear();
        board.gotMoves = false;
        return res;
    }

    // Min-part of MinMax
    public int minimize(Board board, int depth, int foundMax) {
        
        int res = SIGNAL_IMPOSSIBLE;
        int current;
        List<Move> bestMove = null;
        
        int resultingEvaluation;
        
        String transRep;
        
        for (List<Move> mv : board.getMoves()) {
            
            Board resultingBoard = new Board(board);
            resultingBoard.moveUncheckedComplete(mv);
            
            transRep = transpositionRepresentation(resultingBoard);
            
            if (this.transpositionTable.get(transRep) != null) {
                this.transHits++;
                current = this.transpositionTable.get(transRep);
            }
            else {
                resultingEvaluation = this.evaluateBoard(resultingBoard);

                if (depth >= this.maxDepth || resultingEvaluation == this.SIGNAL_WIN || resultingEvaluation == -this.SIGNAL_WIN) {
                    current = resultingEvaluation;
                }
                else {
                    current = this.maximize(resultingBoard, depth + 1, res);
                }
                
                this.transpositionTable.put(transRep, current);
            }
            
            if (current < res) {
                res = current;
                bestMove = mv;
                if (depth == 0 && this.updateEachMove && res < this.chosenMoveEstimation) {
                    this.chosenMove = bestMove;
                    this.chosenMoveEstimation = res;
                    //System.out.println("Updated chosenMove to " + this.chosenMove + ", expecting " + chosenMoveEstimation);
                }
                if (res <= foundMax && (depth != 0 || this.acceptFirstWinningMove)) {
                    break;
                }
            }
            if (depth == 0 && this.checkTimeEachMove && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                if (this.chosenMoveEstimation == this.SIGNAL_IMPOSSIBLE) {
                    this.chosenMove = bestMove;
                    this.chosenMoveEstimation = res;
                }
                this.timeUp = true;
                return res;
            }
        }
        
        if (depth == 0) {
            this.chosenMove = bestMove;
            this.chosenMoveEstimation = res;
            //System.out.println("Finally updated chosenMove to " + this.chosenMove + ", expecting " + chosenMoveEstimation);
        }
        
        if (res == this.SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -this.SIGNAL_WIN) {
            res += depth;
        }
        
        board.moves.clear();
        board.gotMoves = false;
        return res;
    }

    // Not human-friendly, but easily computable and complete representation of a board
    public static String transpositionRepresentation(Board b) {

        return Arrays.deepToString(b.fields);
    }
}
