/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Simplistic Agent just for testing
public class AgentCapture extends Agent {

    @Override
    public List<Move> chooseMove() {
        
        System.out.println("---- AgentCapture Starting calculations ----");
        
        Board workboard = new Board(this.board);
        
        List<List<Move>> moves = workboard.getMoves();
        
        List<Move> res = moves.get(0);
        
        int maxLength = 1;
        for (List<Move> moveList : moves) {
            if (moveList.size() > maxLength) {
                maxLength = moveList.size();
            }
        }
        
        List<List<Move>> resCandidates = new ArrayList<>();
        
        if (maxLength > 1) {
            for (List<Move> moveList : moves) {
                if (moveList.size() >= maxLength) {
                    resCandidates.add(moveList);
                }
            }
        }
        else {
            for (List<Move> moveList : moves) {
                if (moveList.get(moveList.size() - 1).isCapture) {
                    resCandidates.add(moveList);
                }
            }
        }
        
        if (resCandidates.isEmpty()) {
            resCandidates = moves;
        }
        
        Random rand = new Random();
        
        int randIndex = rand.nextInt(resCandidates.size());
        
        res = resCandidates.get(randIndex);
        
        return res;
    }
}
