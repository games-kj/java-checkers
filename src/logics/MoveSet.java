/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

import java.util.List;

/**
 *
 * @author Johannes
 */

// Combination of singular moves
// TODO: Use or remove this class. Currently, combined moves are handled as List<Move>. The benefit of this class would be the additional fields like estimationLongterm, and maybe readability.
public class MoveSet {
    
    public List<Move> steps;
    public int estimationLongterm;
}
