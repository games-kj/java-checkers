/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author Johannes
 */

// Logic representation of a singular move of a piece, mostly without functionality beyond being a data structure
public class Move {
    
    public int fromX;
    public int fromY;
    public int toX;
    public int toY;
    public boolean isCapture;
    public int estimationLongterm;
    
    public Move(int fromX, int fromY, int toX, int toY, boolean isCapture) {
        
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.isCapture = isCapture;
    }
    
    public Move(int fromX, int fromY, int toX, int toY) {
        
        this(fromX, fromY, toX, toY, false);
    }

    @Override
    public String toString() {

        return ( "" + (char)(this.fromX + 97) + (this.fromY + 1) + "-" + (char)(this.toX + 97) + (this.toY + 1));
    }
    
}
