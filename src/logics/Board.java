/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Johannes
 */

// Logical  representation of a checkers board with its pieces, moves, etc.
public class Board {
    
    public boolean forceCapture;
    public boolean flyingQueen;
    public int size;
    public int turn;
    
    public int[][] fields;
    
    public List<List<Move>> moves;
    public boolean gotMoves;
    
    public static final int PIECE_PAWN_WHITE = 1;
    public static final int PIECE_QUEEN_WHITE = 2;
    public static final int PIECE_PAWN_BLACK = -1;
    public static final int PIECE_QUEEN_BLACK = -2;
    public static final int PIECE_NONE = 0;
    
    public Board(boolean forceCapture, boolean flyingQueen, int size) {
        
        this.forceCapture = forceCapture;
        this.flyingQueen = flyingQueen;
        this.size = size;
        
        this.fields = new int[size][size];
        
        int piece = 1;
        for (int y = 0; y < this.size; y++) {
            if (y > 2 && y < this.size - 3) {
                piece = -1;
                continue;
            }
            for (int x = 0; x < this.size; x++) {
                if (((x + y) % 2 == 0)) {
                    this.fields[x][y] = piece;
                }
            }
        }
        
        this.turn = 1;
        this.moves = new ArrayList<>();
        this.gotMoves = false;
    }
    
    public Board() {
        
        this(false, true, 8);
    }

    // Constructor allowing all customizations:
    // 'fields' is the [x][y]-representation of the board, i.e. the fields with the pieces (see constants PIECE_PAWN_WHITE etc.)
    // 'turn' is 1 for white and -1 for black
    // forceCapture refers to the sometimes applied rule that a player has to do a capture if he can
    // flyingQueen refers to the sometimes applied rule that queens can go an arbitrary number of fields in one direction in the first singular move
    // size is the length and height of the (quadratic) board in number of fields
    public Board(int[][] fields, int turn, boolean forceCapture, boolean flyingQueen, int size) {
        
        this(forceCapture, flyingQueen, size);
        this.fields = this.cloneFields(fields);
        this.turn = turn;
    }
    
    public Board(Board board) {
        
        this.forceCapture = board.forceCapture;
        this.flyingQueen = board.flyingQueen;
        this.size = board.size;
        
        this.fields = this.cloneFields(board.fields);
        
        this.turn = board.turn;
        this.moves = new ArrayList<>();
        this.gotMoves = false;
    }

    //  Moves calculation: Get possible moves of the current player
    public void calculateMoves() {
        
        this.gotMoves = true;
        this.moves.clear();
        
        int[][] fieldsCopy = this.cloneFields(this.fields);
        
        int distance;
        
        for (int y = 0; y < this.size; y++) {
            for (int x = 0; x < this.size; x++) {

                // Only calculate moves of pieces of the current player
                // TODO: Maybe create a function for this determination that does not rely on specific values of PIECE_PAWN_WHITE etc. The current solution is probably the most performant, but it may not make much difference.
                if (this.fields[x][y] * this.turn <= 0) continue;

                // If singular moves to the neighboring fields are relevant for the current piece (i.e. not a queen in flyingQueen mode): Calculate them
                // TODO: Weed out queen moves in this case
                if (!(this.flyingQueen && this.fields[x][y] * this.fields[x][y] > 1)) {
                    for (int directionY = 1; directionY >= (this.fields[x][y] * this.fields[x][y] == 1 ? 1 : -1); directionY -= 2) {
                        for (int aimX = x-1; aimX <= x+1; aimX += 2) {
                            if (aimX >= 0 && aimX < this.size && y + turn * directionY >= 0 && y + turn * directionY < this.size && this.fields[aimX][y + turn * directionY] == 0) {
                                List<Move> moveList = new ArrayList<Move>();
                                moveList.add(new Move(x, y, aimX, y + turn * directionY));
                                this.moves.add(moveList);
                            }
                        }
                    }
                }

                // Recursively add jumping (capturing) moves
                this.moves.addAll(jumpsFrom(x, y, fieldsCopy));

                // If queen in flyingQueen mode: first singular move can span an arbitrary number of fields
                // For each of the for diagonal directions, move along farther step by step and check collisions and possible jumps for each field
                if (this.flyingQueen && this.fields[x][y] * this.fields[x][y] > 1) {
                    for (int directionY = 1; directionY >= -1; directionY -= 2) {
                        for (int directionX = 1; directionX >= -1; directionX -= 2) {
                            distance = 1;
                            while (true) {
                                if (x + directionX * distance < 0 || x + directionX * distance >= this.size || y + directionY * distance < 0 || y + directionY * distance >= this.size || this.fields[x + directionX * distance][y + directionY * distance] != 0) break;
                                List<Move> moveListDirect = new ArrayList<Move>();
                                Move startmove = new Move(x, y, x + directionX * distance, y + directionY * distance);
                                moveListDirect.add(startmove);
                                this.moves.add(moveListDirect);
                                
                                int[][] fieldsCopy2 = this.cloneFields(this.fields);
                                fieldsCopy2[x + directionX * distance][y + directionY * distance] = fieldsCopy2[x][y];
                                fieldsCopy2[x][y] = 0;
                                List<List<Move>> followingMoveLists = this.jumpsFrom(x + directionX * distance, y + directionY * distance, fieldsCopy);
                                
                                for (List<Move> followingMoveList : followingMoveLists) {
                                    List<Move> moveList = new ArrayList<Move>();
                                    moveList.add(startmove);
                                    moveList.addAll(followingMoveList);
                                    this.moves.add(moveList);
                                }
                                
                                distance++;
                            }
                        }
                    }
                }
            }
        }

        // If the forceCapture rule is set: Check if there are capture moves, and if so, remove all non-capture moves.
        if (this.forceCapture) {
            
            List<List<Move>> captureMoves = new ArrayList<>();
            
            for (List<Move> move : this.moves) {
                if (move.get(0).isCapture) {
                    captureMoves.add(move);
                }
            }
            
            if (!captureMoves.isEmpty()) {
                this.moves = captureMoves;
            }
        }
    }

    // Possible jump moves from the field [x][y], including follow-up jumps by a recursive call
    // A move combination is a List<Move>. Therefore, multiple possible jumps are a List<List<Move>>
    // As these possibilities can split into further possibilities after the first singular moves, a tree-like structure results
    public List<List<Move>> jumpsFrom(int x, int y, int[][] fields) {
        
        List<List<Move>> res = new ArrayList<>();
        
        //System.out.println("Calculating jumps from: ");
        //this.printFields(fields);

        // Go through 2x2=4 diagonal directions
        for (int directionY = 1; directionY >= (fields[x][y] * fields[x][y] == 1 ? 1 : -1); directionY -= 2) {
            for (int directionX = -1; directionX <= 1; directionX += 2) {
                // If resulting field is valid and reachable by a jump: Do calculations
                if (x + 2 * directionX >= 0 && x + 2 * directionX < this.size && y + 2 * turn * directionY < this.size && y + 2 * turn * directionY >= 0 && fields[x + directionX][y + turn * directionY] * this.turn < 0 && fields[x + 2 * directionX][y + 2 * turn * directionY] == 0) {
                    Move startmove = new Move(x, y, x + 2 * directionX, y + 2 * turn * directionY, true);
                    // Will need field occupations after singular move for recursive call
                    int[][] fieldsCopy = this.cloneFields(fields);
                    fieldsCopy[x + 2 * directionX][y + 2 * turn * directionY] = fieldsCopy[x][y];
                    fieldsCopy[x][y] = 0;
                    fieldsCopy[x + directionX][y + turn * directionY] = 0;
                    //System.out.println("Can jump from (" + x + "/" + y + ") to (" + (x + 2 * directionX) + "/" + (y + 2 * turn * directionY) + "), result:");
                    //this.printFields(fieldsCopy);
                    // Recursive call
                    List<List<Move>> followingMoveLists = this.jumpsFrom(x + 2 * directionX, y + 2 * turn * directionY, fieldsCopy);

                    List<Move> moveListDirect = new ArrayList<Move>();
                    moveListDirect.add(startmove);
                    res.add(moveListDirect);

                    for (List<Move> followingMoveList : followingMoveLists) {
                        List<Move> moveList = new ArrayList<Move>();
                        moveList.add(startmove);
                        moveList.addAll(followingMoveList);
                        res.add(moveList);
                    }
                }
            }
        }
        
        return res;
    }

    // Cached moves calculation function
    public List<List<Move>> getMoves() {
        
        if (!this.gotMoves) {
            this.calculateMoves();
        }
        
        return this.moves;
    }

    // Apply singular move without checking if it is valid
    // 'finishTurn' has to be false for the non-finishing singular moves of combined moves
    public void moveUnchecked(Move move, boolean finishTurn) {
        
        this.fields[move.toX][move.toY] = this.fields[move.fromX][move.fromY];
        this.fields[move.fromX][move.fromY] = 0;
        
        if (move.isCapture) {
            this.fields[(move.fromX + move.toX) / 2][(move.fromY + move.toY) / 2] = 0;
        }
        
        //System.out.println("Turn " + this.turn + " moving from (" + move.fromX + "/" +  move.fromY + ") to (" + move.toX + "/" + move.toY + ")");
        
        if (move.toY == 0 || move.toY == this.size - 1) {
            this.fields[move.toX][move.toY] = this.turn * 2;
            //System.out.println("New Queen for turn " + this.turn + " signalized by " + this.fields[move.toX][move.toY]);
        }
        
        this.gotMoves = false;
        
        if (finishTurn) {
            this.turn *= -1;
        }
    }
    
    public void moveUnchecked(Move move) {
        
        this.moveUnchecked(move, true);
    }

    // Apply combined move without checking if it is valid
    public void moveUncheckedComplete(List<Move> moves) {
        
        for (Move move : moves) {
            this.moveUnchecked(move, false);
        }
        
        this.turn *= -1;
    }

    // Apply singular move if it is valid
    // TODO: Implement checking or remove function. This function was created to copy the structure of the chess project, but currently is not sensible
    public boolean move(Move move, boolean finishTurn) {
        
        this.moveUnchecked(move, finishTurn);
        
        return true;
    }

    public boolean move(Move move) {
        
        return this.move(move, true);
    }

    // Apply combined move if it is valid
    public boolean moveComplete(List<Move> moves) {
        
        List<Move> adjustedMove = this.adjustMove(moves);
        
        if (adjustedMove == null) return false;
        
        this.moveUncheckedComplete(adjustedMove);
        return true;
    }

    // Check if combined move is in calculated possible moves (i.e. if it is a valid move). If so, return the calculated move, otherwise null
    // This is relevant for moves from external input and allows for validity checking, auto-filling of non-coordinate fields and pointer equality
    public List<Move> adjustMove(List<Move> moves) {
        
        for (Move mv : moves) {
            if (mv.fromX < 0 || mv.fromX >= this.size || mv.fromY < 0 || mv.fromY >= this.size || mv.toX < 0 || mv.toX >= this.size || mv.toY < 0 || mv.toY >= this.size) {
                return null;
            }
        }
        boolean match;
        
        for (List<Move> mvPossible : this.getMoves()) {
            if (mvPossible.size() != moves.size()) continue;
            match = true;
            for (int i = 0; i < mvPossible.size(); i++) {
                if (mvPossible.get(i).fromX != moves.get(i).fromX || mvPossible.get(i).fromY != moves.get(i).fromY || mvPossible.get(i).toX != moves.get(i).toX || mvPossible.get(i).toY != moves.get(i).toY) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return mvPossible;
            }
        }
        
        return null;
    }

    // Basic 2D array cloning
    public int[][] cloneFields(int[][] fields) {
        
        int[][] res = new int[this.size][this.size];
        
        for (int y = 0; y < this.size; y++) {
            for (int x = 0; x < this.size; x++) {
                res[x][y] = fields[x][y];
            }
        }
        
        return res;
    }
    
    public void printFields(int[][] fields) {
        
        for (int y = this.size - 1; y >= 0; y--) {
            for (int x = 0; x < this.size; x++) {
                System.out.print(fields[x][y] + (fields[x][y] >= 0 ? "   " : "  "));
            }
            System.out.println("");
        }
    }
}
