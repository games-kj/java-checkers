/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static functions.Functions.parseCharBoard;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a board, the color of the player ('w' for white, 'b' for black), forceCapture rule (0 = false), flyingQueen rule (0 = false) and move as arguments, applies the move to the board, and prints the results
// TODO: Documentation of input and output format and parsing details
public class DoMoveCli {
    
    public static void main(String[] args) {
        
        int[][] fields;
        boolean forceCapture;
        boolean flyingQueen;
        int size = -1;
        int turn;
        String moveTxt;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            fields = parseCharBoard(args[0]);
            turn = (args[1].equals("w") ? 1 : -1);
            forceCapture = Integer.parseInt(args[2]) > 0;
            flyingQueen = Integer.parseInt(args[3]) > 0;
            moveTxt = args[4];
            
            for (int[] col : fields) {
                if (size < 0) {
                    size = col.length;
                }
                else if (col.length != size) {
                    throw new Exception();
                }
            }
            
        }
        catch(Exception e) {
            System.out.println("error 1");
            return;
        }
        
        Board board;
        String endresult = "o";
        List<List<Move>> returnedMovesPossible;
        
        try {
            String[] stages = moveTxt.split("-");
            List<Move> sequence = new ArrayList();
            
            for (int i = 1; i < stages.length; i++) {
                
                int xPrev = Character.getNumericValue(stages[i-1].charAt(0)) - 10;
                int yPrev = Integer.parseInt(stages[i-1].substring(1)) - 1;
                int x = Character.getNumericValue(stages[i].charAt(0)) - 10;
                int y = Integer.parseInt(stages[i].substring(1)) - 1;
                
                sequence.add(new Move(xPrev, yPrev, x, y));
            }
            
            board = new Board(fields, turn, forceCapture, flyingQueen, size);
            
            if (!board.moveComplete(sequence)) throw new RuntimeException("Move rejected by board. Original: " + moveTxt);
            
            returnedMovesPossible = board.getMoves();
            
            if (returnedMovesPossible.isEmpty()) {
                endresult = (turn == 1 ? "w" : "b");
            }
        }
        catch(Exception e) {
            System.out.println("error 2: " + e.getMessage());
            return;
        }
        
        try {
            String textMovesPossible = returnedMovesPossible.stream().map(sequence -> "\"" + sequence.stream().map(move -> move.toString()).collect(Collectors.joining("-")) + "\"").collect(Collectors.toList()).toString().replace(" ", "").replaceAll("([a-z]\\d+-)\\1+", "$1");
            String textBoard = Arrays.deepToString(board.fields).replace("0", "\"0\"").replace("-1", "\"b\"").replace("-2", "\"B\"").replace("1", "\"w\"").replace("2", "\"W\"").replace(" ", "");
            
            System.out.println("board: " + textBoard);
            System.out.println("moves: " + textMovesPossible);
            System.out.println("endresult: " + endresult);
        }
        catch(Exception e) {
            System.out.println("error 3");
        }
    }
    
}
