/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import agents.AgentMinMax1;
import static functions.Functions.parseCharBoard;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a board, the color of the player ('w' for white, 'b' for black), forceCapture rule (0 = false), flyingQueen rule (0 = false) and the number of ms to think as arguments, chooses a move, applies the move to the board and prints the results
// TODO: Documentation of input and output format and parsing details
public class AgentCli {
    
    public static void main(String[] args) {
        
        int[][] fields;
        boolean forceCapture;
        boolean flyingQueen;
        int size = -1;
        int turn;
        int allowedMs;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            fields = parseCharBoard(args[0]);
            turn = (args[1].equals("w") ? 1 : -1);
            forceCapture = Integer.parseInt(args[2]) > 0;
            flyingQueen = Integer.parseInt(args[3]) > 0;
            allowedMs = Integer.parseInt(args[4]);
            
            if (allowedMs < 100 || allowedMs > 60000) {
                throw new Exception("Invalid number of ms: " + allowedMs);
            }
            
            for (int[] col : fields) {
                if (size < 0) {
                    size = col.length;
                }
                else if (col.length != size) {
                    throw new Exception("Inconsistent boardsize");
                }
            }
            
        }
        catch(Exception e) {
            System.out.println("error 1: " + Arrays.deepToString(e.getStackTrace()));
            return;
        }
        
        List<Move> agentMove;
        String endresult = "o";
        List<List<Move>> returnedMovesPossible;
        Board board;
        
        try {
            board = new Board(fields, turn, forceCapture, flyingQueen, size);
            
            AgentMinMax1 agent = new AgentMinMax1();
            
            agent.init(board, turn);
            agent.allowedMillis = allowedMs;
            
            agentMove = agent.chooseMove();
            
            if (!board.moveComplete(agentMove)) {
                throw new Exception();
            }
            
            returnedMovesPossible = board.getMoves();
            
            if (board.getMoves().isEmpty()) {
                endresult = (turn == 1 ? "w" : "b");
            }
        }
        catch(Exception e) {
            System.out.println("error 2");
            return;
        }
        
        try {
            String textMovesPossible = returnedMovesPossible.stream().map(sequence -> "\"" + sequence.stream().map(move -> move.toString()).collect(Collectors.joining("-")) + "\"").collect(Collectors.toList()).toString().replace(" ", "").replaceAll("([a-z]\\d+-)\\1+", "$1");
            String textAgentMove = agentMove.stream().map(move -> move.toString()).collect(Collectors.joining("-")).replaceAll("([a-z]\\d+-)\\1+", "$1");
            String textBoard = Arrays.deepToString(board.fields).replace("0", "\"0\"").replace("-1", "\"b\"").replace("-2", "\"B\"").replace("1", "\"w\"").replace("2", "\"W\"").replace(" ", "");
            
            System.out.println("board: " + textBoard);
            System.out.println("move: " + textAgentMove);
            System.out.println("moves: " + textMovesPossible);
            System.out.println("endresult: " + endresult);
        }
        catch(Exception e) {
            System.out.println("error 3");
        }
    }
}
