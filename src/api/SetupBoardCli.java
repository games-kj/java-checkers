/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.util.Arrays;
import logics.Board;

/**
 *
 * @author Johannes
 */

// Accepts the size of the (quadratic) board as a parameter and prints the board in initial state
// TODO: Documentation of input and output format and parsing details
public class SetupBoardCli {
    
    public static void main(String[] args) {
        
        int size;
        
        try {
            size = Integer.parseInt(args[0]);
        }
        catch(Exception e) {
            System.out.println("-1");
            return;
        }
        
        Board board = new Board(false, false, size);
        
        String[][] resFields = new String[size][size];
        
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                switch (board.fields[x][y]) {
                    case 0:
                        resFields[x][y] = "\"0\"";
                        break;
                    case 1:
                        resFields[x][y] = "\"w\"";
                        break;
                    case -1:
                        resFields[x][y] = "\"b\"";
                        break;
                    case 2:
                        resFields[x][y] = "\"W\"";
                        break;
                    case -2:
                        resFields[x][y] = "\"B\"";
                        break;
                }
            }
        }
        
        System.out.println(Arrays.deepToString(resFields).replace(" ", ""));
    }
}
