/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static functions.Functions.parseCharBoard;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a board, the color of the player ('w' for white, 'b' for black), forceCapture rule (0 = false) and flyingQueen rule (0 = false) as parameters, prints the possible moves and status of the board
// TODO: Documentation of input and output format and parsing details
public class StatusCli {
    
    public static void main(String[] args) {
        
        int[][] fields;
        boolean forceCapture;
        boolean flyingQueen;
        int size = -1;
        int turn;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            fields = parseCharBoard(args[0]);
            turn = (args[1].equals("w") ? 1 : -1);
            forceCapture = Integer.parseInt(args[2]) > 0;
            flyingQueen = Integer.parseInt(args[3]) > 0;
            
            for (int[] col : fields) {
                if (size < 0) {
                    size = col.length;
                }
                else if (col.length != size) {
                    throw new Exception();
                }
            }
            
        }
        catch(Exception e) {
            System.out.println("error 1");
            return;
        }
        
        String endresult = "o";
        List<List<Move>> returnedMovesPossible;
        Board board;
        
        try {
            board = new Board(fields, turn, forceCapture, flyingQueen, size);
            
            returnedMovesPossible = board.getMoves();
            
            if (board.getMoves().isEmpty()) {
                endresult = (turn == 1 ? "b" : "w");
            }
        }
        catch(Exception e) {
            System.out.println("error 2");
            return;
        }
        
        try {
            String textMovesPossible = returnedMovesPossible.stream().map(sequence -> "\"" + sequence.stream().map(move -> move.toString()).collect(Collectors.joining("-")) + "\"").collect(Collectors.toList()).toString().replace(" ", "").replaceAll("([a-z]\\d+-)\\1+", "$1");
            
            System.out.println("moves: " + textMovesPossible);
            System.out.println("endresult: " + endresult);
        }
        catch(Exception e) {
            System.out.println("error 3");
        }
    }
}
