/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import logics.Board;

/**
 *
 * @author Johannes
 */

// Graphical frame for correct visualization of the graphical board
public class GFrame extends JFrame {
    
    public int fieldsize = 100;
    public int coinsize = 70;
    public int queensize = 90;
    public Board board;
    public GBoard gBoard;
    
    public GFrame(Board board) {
        
        this.board = board;
        this.gBoard = new GBoard(this);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.add(this.gBoard);
        this.pack();
    }
    
    public void update() {
        
        this.repaint();
    }
    
    public void close() {
        
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
