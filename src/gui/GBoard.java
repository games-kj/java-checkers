/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Johannes
 */

// Graphical board representation
public class GBoard extends JPanel {
    
    public GFrame frame;
    
    public GBoard(GFrame frame) {
        
        this.frame = frame;
        this.setPreferredSize(new Dimension(this.frame.board.size * this.frame.fieldsize, this.frame.board.size * this.frame.fieldsize));
        this.setVisible(true);
    }
    
    @Override
    public void paint(Graphics g) {
        
        for (int y = 0; y < this.frame.board.size; y++) {
            for (int x = 0; x < this.frame.board.size; x++) {
                g.setColor(((x + y) % 2 == 0) ? Color.lightGray : Color.darkGray);
                g.fillRect(x * this.frame.fieldsize, y * this.frame.fieldsize, this.frame.fieldsize, this.frame.fieldsize);
            }
        }
        int piecesize;
        for (int y = 0; y < this.frame.board.size; y++) {
            for (int x = 0; x < this.frame.board.size; x++) {
                if (this.frame.board.fields[x][y] == 0) continue;
                piecesize = ((this.frame.board.fields[x][y] * this.frame.board.fields[x][y] == 1 ? this.frame.coinsize : this.frame.queensize));
                g.setColor(this.frame.board.fields[x][y] > 0 ? Color.white : Color.black);
                g.fillOval(x * this.frame.fieldsize + this.frame.fieldsize / 2 - piecesize / 2, (this.frame.board.size - y - 1) * this.frame.fieldsize + this.frame.fieldsize / 2 - piecesize / 2, piecesize, piecesize);
            }
        }
    }
}
