/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import agents.Agent;
import agents.AgentMinMax1;
import agents.Human;
import gui.GFrame;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Represents a game of checkers, letting two agents compete and managing a GUI
public class Game {
    
    public GFrame gui;
    public Board board;
    
    public Agent player1;
    public Agent player2;
    public int turn = 1;
    public int winner = 0;
    
    public Game(Agent player1, Agent player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public void start(){
        
        if (this.gui != null) {
            this.gui.close();
        }
        
        this.board = new Board(false, false, 8);
        
        this.player1.init(board, 1);
        this.player2.init(board, -1);
        
        this.gui = new GFrame(this.board);
        this.gui.update();
        
        while(true) {
            this.doTurn();
            this.gui.update();
            if (this.board.getMoves().isEmpty()) {
                this.winner = this.turn * (-1);
                break;
            }
        }
        
        System.out.println("WINNER: player" + (this.winner == 1 ? "1" : "2"));
    }
    
    public void doTurn() {
        
        List<Move> moveList;
        List<Move> adjustedMove;
        
        while(true) {
            moveList = (turn == 1 ? player1.chooseMove() : player2.chooseMove());
            adjustedMove = this.board.adjustMove(moveList);
            
            if (adjustedMove != null) {
                break;
            }
            
            System.out.println("Board rejected the move");
        }
        
        for (Move move : adjustedMove) {
            
            board.moveUnchecked(move, false);
            this.gui.update();
            try {
                TimeUnit.MILLISECONDS.sleep(1000);

            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        board.turn *= -1;
        
        this.turn *= -1;
    }

    // If you use this as the project's main function, you can set which players will compete here
    public static void main(String[] args) {
        Agent player2 = new AgentMinMax1();
        Agent player1 = new Human();
        Game game = new Game(player1, player2);
        game.start();
    }
}
